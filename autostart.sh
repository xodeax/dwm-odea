#!/bin/bash
wmname LG3D &
nitrogen --restore &
dwmblocks &
compton &
sleep 1 && firefox &
sleep 1 && thunderbird &
vdirsyncer sync &
/home/odea/.local/bin/getfbevents &
conky &
